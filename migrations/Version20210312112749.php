<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312112749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE horaire (id INT AUTO_INCREMENT NOT NULL, heure_debut TIME NOT NULL, heure_fin TIME NOT NULL, jour DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horaire_matiere (horaire_id INT NOT NULL, matiere_id INT NOT NULL, INDEX IDX_A7DD085958C54515 (horaire_id), INDEX IDX_A7DD0859F46CD258 (matiere_id), PRIMARY KEY(horaire_id, matiere_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE horaire_matiere ADD CONSTRAINT FK_A7DD085958C54515 FOREIGN KEY (horaire_id) REFERENCES horaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE horaire_matiere ADD CONSTRAINT FK_A7DD0859F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE emploi_du_temps_matiere');
        $this->addSql('ALTER TABLE emploi_du_temps DROP heure_debut, DROP heure_fin, DROP jour');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE horaire_matiere DROP FOREIGN KEY FK_A7DD085958C54515');
        $this->addSql('CREATE TABLE emploi_du_temps_matiere (emploi_du_temps_id INT NOT NULL, matiere_id INT NOT NULL, INDEX IDX_77DBA1E0F46CD258 (matiere_id), INDEX IDX_77DBA1E0C13CD51E (emploi_du_temps_id), PRIMARY KEY(emploi_du_temps_id, matiere_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere ADD CONSTRAINT FK_77DBA1E0C13CD51E FOREIGN KEY (emploi_du_temps_id) REFERENCES emploi_du_temps (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere ADD CONSTRAINT FK_77DBA1E0F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE horaire');
        $this->addSql('DROP TABLE horaire_matiere');
        $this->addSql('ALTER TABLE emploi_du_temps ADD heure_debut TIME NOT NULL, ADD heure_fin TIME NOT NULL, ADD jour DATE NOT NULL');
    }
}
