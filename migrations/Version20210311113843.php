<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210311113843 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE actualite (id INT AUTO_INCREMENT NOT NULL, carousels_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, soustitre VARCHAR(255) NOT NULL, texte LONGTEXT DEFAULT NULL, image VARCHAR(100) DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5492819719646808 (carousels_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carousel (id INT AUTO_INCREMENT NOT NULL, active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE classe (id INT AUTO_INCREMENT NOT NULL, etablissement_id INT DEFAULT NULL, emploi_du_temps_id INT DEFAULT NULL, nom_classe VARCHAR(5) NOT NULL, INDEX IDX_8F87BF96FF631228 (etablissement_id), UNIQUE INDEX UNIQ_8F87BF96C13CD51E (emploi_du_temps_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussion (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussion_user (discussion_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A8FD7A7F1ADED311 (discussion_id), INDEX IDX_A8FD7A7FA76ED395 (user_id), PRIMARY KEY(discussion_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emploi_du_temps (id INT AUTO_INCREMENT NOT NULL, heure_debut DATETIME NOT NULL, heure_fin DATETIME NOT NULL, jour DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emploi_du_temps_matiere (emploi_du_temps_id INT NOT NULL, matiere_id INT NOT NULL, INDEX IDX_77DBA1E0C13CD51E (emploi_du_temps_id), INDEX IDX_77DBA1E0F46CD258 (matiere_id), PRIMARY KEY(emploi_du_temps_id, matiere_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etablissement (id INT AUTO_INCREMENT NOT NULL, nom_etablissement VARCHAR(255) NOT NULL, image VARCHAR(100) NOT NULL, updated_at DATETIME NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, matiere_id INT DEFAULT NULL, INDEX IDX_1323A575F46CD258 (matiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, nom_matiere VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, discussions_id INT DEFAULT NULL, envoyeur_id INT DEFAULT NULL, receveur_id INT DEFAULT NULL, sujet VARCHAR(255) NOT NULL, date_message DATETIME NOT NULL, contenu LONGTEXT NOT NULL, piece_jointe VARCHAR(255) DEFAULT NULL, INDEX IDX_B6BD307F8DDB4304 (discussions_id), INDEX IDX_B6BD307F4795A786 (envoyeur_id), INDEX IDX_B6BD307FB967E626 (receveur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note (id INT AUTO_INCREMENT NOT NULL, evaluations_id INT DEFAULT NULL, utilisateurs_id INT DEFAULT NULL, note DOUBLE PRECISION NOT NULL, appreciations LONGTEXT DEFAULT NULL, INDEX IDX_CFBDFA14788B35D6 (evaluations_id), INDEX IDX_CFBDFA141E969C5 (utilisateurs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, classes_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, prenom VARCHAR(100) NOT NULL, nom VARCHAR(100) NOT NULL, date_naissance DATE DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(10) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(5) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, role VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), INDEX IDX_8D93D6499E225B24 (classes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE actualite ADD CONSTRAINT FK_5492819719646808 FOREIGN KEY (carousels_id) REFERENCES carousel (id)');
        $this->addSql('ALTER TABLE classe ADD CONSTRAINT FK_8F87BF96FF631228 FOREIGN KEY (etablissement_id) REFERENCES etablissement (id)');
        $this->addSql('ALTER TABLE classe ADD CONSTRAINT FK_8F87BF96C13CD51E FOREIGN KEY (emploi_du_temps_id) REFERENCES emploi_du_temps (id)');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere ADD CONSTRAINT FK_77DBA1E0C13CD51E FOREIGN KEY (emploi_du_temps_id) REFERENCES emploi_du_temps (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere ADD CONSTRAINT FK_77DBA1E0F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F8DDB4304 FOREIGN KEY (discussions_id) REFERENCES discussion (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F4795A786 FOREIGN KEY (envoyeur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FB967E626 FOREIGN KEY (receveur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14788B35D6 FOREIGN KEY (evaluations_id) REFERENCES evaluation (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA141E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499E225B24 FOREIGN KEY (classes_id) REFERENCES classe (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actualite DROP FOREIGN KEY FK_5492819719646808');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499E225B24');
        $this->addSql('ALTER TABLE discussion_user DROP FOREIGN KEY FK_A8FD7A7F1ADED311');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F8DDB4304');
        $this->addSql('ALTER TABLE classe DROP FOREIGN KEY FK_8F87BF96C13CD51E');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere DROP FOREIGN KEY FK_77DBA1E0C13CD51E');
        $this->addSql('ALTER TABLE classe DROP FOREIGN KEY FK_8F87BF96FF631228');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14788B35D6');
        $this->addSql('ALTER TABLE emploi_du_temps_matiere DROP FOREIGN KEY FK_77DBA1E0F46CD258');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575F46CD258');
        $this->addSql('ALTER TABLE discussion_user DROP FOREIGN KEY FK_A8FD7A7FA76ED395');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F4795A786');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FB967E626');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA141E969C5');
        $this->addSql('DROP TABLE actualite');
        $this->addSql('DROP TABLE carousel');
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE discussion');
        $this->addSql('DROP TABLE discussion_user');
        $this->addSql('DROP TABLE emploi_du_temps');
        $this->addSql('DROP TABLE emploi_du_temps_matiere');
        $this->addSql('DROP TABLE etablissement');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE user');
    }
}
