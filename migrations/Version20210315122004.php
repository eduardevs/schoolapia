<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315122004 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F4795A786');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FB967E626');
        $this->addSql('DROP INDEX IDX_B6BD307FB967E626 ON message');
        $this->addSql('DROP INDEX IDX_B6BD307F4795A786 ON message');
        $this->addSql('ALTER TABLE message DROP envoyeur_id, DROP receveur_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ADD envoyeur_id INT DEFAULT NULL, ADD receveur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F4795A786 FOREIGN KEY (envoyeur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FB967E626 FOREIGN KEY (receveur_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B6BD307FB967E626 ON message (receveur_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F4795A786 ON message (envoyeur_id)');
    }
}
